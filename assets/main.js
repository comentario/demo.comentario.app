((doc) => {
    addEventListener('load', () => {

        const elThemeSwitcher = doc.getElementById('theme-switcher');
        const elComments      = doc.getElementsByTagName('comentario-comments')[0];

        const setTheme = (th) => {
            doc.documentElement.setAttribute('data-bs-theme', th);
            elComments.setAttribute('theme', th);
            // Store the preference
            localStorage.setItem('theme', th);
        };

        const initTheme = () => {
            let th = localStorage.getItem('theme');
            if (!['light', 'dark'].includes(th)){
                th = 'light';
            }

            // Sync the switch
            elThemeSwitcher.checked = th === 'dark';

            // Activate the theme
            setTheme(th);
        }

        // Init the theme
        initTheme();

        // Bind handler to the theme switcher
        elThemeSwitcher.addEventListener('click', () => setTheme(elThemeSwitcher.checked ? 'dark' : 'light'));
    });
})(document);
