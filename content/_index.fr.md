---
title: Comentario Démonstration
heading: Bienvenue sur la page de démonstration de Comentario !
---

{{< div "text-center" >}}
[Comentario](https://comentario.app/) est un puissant moteur de commentaires open-source pour les pages web.

Cette page montre comment ajouter des commentaires à un site web.

N'oubliez pas de jeter un coup d'oeil à notre superbe [Admin UI](https://edge.comentario.app/en/manage/dashboard): connectez-vous avec l'email `admin@admin` et le mot de passe `admin`.

{{< demo-buttons >}}
{{< /div >}}

{{< comments >}}
