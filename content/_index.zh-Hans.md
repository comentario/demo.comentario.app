---
title: 评论演示
heading: 欢迎来到 Comentario 演示页面！
---

{{< div "text-center" >}}
[Comentario](https://comentario.app/)是一个功能强大的开源网页评论引擎。

本页演示了如何在网站上添加评论。

别忘了看看我们出色的 [Admin UI](https://edge.comentario.app/en/manage/dashboard)：使用电子邮件 `admin@admin` 和密码 `admin` 登录。

{{< demo-buttons >}}
{{< /div >}}

{{< comments >}}
