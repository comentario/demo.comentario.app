---
title: Демо-сайт Comentario
heading: Демо-сайт Comentario категорически вас приветствует!
---

{{< div "text-center" >}}
[Comentario](https://comentario.app/) — это мощный опенсорсный движок комментариев для веб-страниц.

Здесь показано, как могут выглядеть комментарии на странице.

Кстати, не забудьте заглянуть и в [Панель администратора](https://edge.comentario.app/en/manage/dashboard): там вход с емэйлом `admin@admin` и паролем `admin`.

{{< demo-buttons >}}
{{< /div >}}

{{< comments >}}
