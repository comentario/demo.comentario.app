---
title: 評論演示
heading: 歡迎來到 Comentario 的演示頁面！
---

{{< div "text-center" >}}
[Comentario](https://comentario.app/) 是一個功能強大、開放源碼的網頁評論引擎。

本頁面示範如何在網站上新增註解。

別忘了看看我們很棒的 [Admin UI](https://edge.comentario.app/en/manage/dashboard)：使用電子郵件 `admin@admin` 和密碼 `admin` 登入。

{{< demo-buttons >}}
{{< /div >}}

{{< comments >}}
