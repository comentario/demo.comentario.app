---
title: Comentario Demo
heading: Welkom bij de demopagina van Comentario!
---

{{< div "text-center" >}}
[Comentario](https://comentario.app/) is een krachtige opensource comment-engine voor webpagina's.

Deze pagina demonstreert hoe je comments aan een website kunt toevoegen.

Vergeet ook niet onze [Admin UI](https://edge.comentario.app/en/manage/dashboard): log in met e-mailadres `admin@admin` en wachtwoord `admin`.

{{< demo-buttons >}}
{{< /div >}}

{{< comments >}}
