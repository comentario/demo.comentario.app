---
title: Comentario Demo
heading: ¡Bienvenido a la página de demostración de Comentario!
---

{{< div "text-center" >}}
[Comentario](https://comentario.app/) es un potente motor de comentarios de código abierto para páginas web.

Esta página muestra cómo añadir comentarios a un sitio web.

No olvide echar un vistazo a nuestra gran [Admin UI](https://edge.comentario.app/en/manage/dashboard): inicie sesión con el correo electrónico `admin@admin` y la contraseña `admin`.

{{< demo-buttons >}}
{{< /div >}}

{{< comments >}}
