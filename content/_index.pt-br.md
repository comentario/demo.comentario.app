---
title: Comentario Demo
heading: Bem-vindo à página de demonstração do Comentario!
---

{{< div "text-center" >}}
O [Comentario](https://comentario.app/) é um mecanismo de comentários avançado e de código aberto para páginas da Web.

Esta página demonstra como você pode adicionar comentários a um site.

Não se esqueça de dar uma olhada na nossa excelente [Admin UI](https://edge.comentario.app/en/manage/dashboard): faça login com o e-mail `admin@admin` e a senha `admin`.

{{< demo-buttons >}}
{{< /div >}}

{{< comments >}}
