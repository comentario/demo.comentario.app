---
title: Comentario Demo
heading: Welcome to the demo page of Comentario!
---

{{< div "text-center" >}}
[Comentario](https://comentario.app/) is a powerful, open-source comment engine for web pages.

This page demonstrates how you can add comments to a website.

Don't forget to have a look at our great [Admin UI](https://edge.comentario.app/en/manage/dashboard): login with email `admin@admin` and password `admin`.

{{< demo-buttons >}}
{{< /div >}}

{{< comments >}}
